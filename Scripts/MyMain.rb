
SCRIPTS = [
	# +==============================+
	# |           Helpers            |
	# +==============================+
	"MyHelpers.rb",
	
	# +==============================+
	# |           Modules            |
	# +==============================+
	"Engine/Modules/Vocab.rb",
	"Engine/Modules/Sound.rb",
	"Engine/Modules/Cache.rb",
	"Engine/Modules/DataManager.rb",
	"Engine/Modules/SceneManager.rb",
	"Engine/Modules/BattleManager.rb",
	
	# +==============================+
	# |         GameObjects          |
	# +==============================+
	"Engine/GameObjects/Game_Temp.rb",
	"Engine/GameObjects/Game_System.rb",
	"Engine/GameObjects/Game_Timer.rb",
	"Engine/GameObjects/Game_Message.rb",
	"Engine/GameObjects/Game_Switches.rb",
	"Engine/GameObjects/Game_Variables.rb",
	"Engine/GameObjects/Game_SelfSwitches.rb",
	"Engine/GameObjects/Game_Screen.rb",
	"Engine/GameObjects/Game_Picture.rb",
	"Engine/GameObjects/Game_Pictures.rb",
	"Engine/GameObjects/Game_BaseItem.rb",
	"Engine/GameObjects/Game_Action.rb",
	"Engine/GameObjects/Game_ActionResult.rb",
	"Engine/GameObjects/Game_BattlerBase.rb",
	"Engine/GameObjects/Game_Battler.rb",
	"Engine/GameObjects/Game_Actor.rb",
	"Engine/GameObjects/Game_Enemy.rb",
	"Engine/GameObjects/Game_Actors.rb",
	"Engine/GameObjects/Game_Unit.rb",
	"Engine/GameObjects/Game_Party.rb",
	"Engine/GameObjects/Game_Troop.rb",
	"Engine/GameObjects/Game_Map.rb",
	"Engine/GameObjects/Game_CommonEvent.rb",
	"Engine/GameObjects/Game_CharacterBase.rb",
	"Engine/GameObjects/Game_Character.rb",
	"Engine/GameObjects/Game_Player.rb",
	"Engine/GameObjects/Game_Follower.rb",
	"Engine/GameObjects/Game_Followers.rb",
	"Engine/GameObjects/Game_Vehicle.rb",
	"Engine/GameObjects/Game_Event.rb",
	"Engine/GameObjects/Game_Interpreter.rb",
	
	# +==============================+
	# |           Sprites            |
	# +==============================+
	"Engine/Sprites/Sprite_Base.rb",
	"Engine/Sprites/Sprite_Character.rb",
	"Engine/Sprites/Sprite_Battler.rb",
	"Engine/Sprites/Sprite_Picture.rb",
	"Engine/Sprites/Sprite_Timer.rb",
	"Engine/Sprites/Spriteset_Weather.rb",
	"Engine/Sprites/Spriteset_Map.rb",
	"Engine/Sprites/Spriteset_Battle.rb",
	
	# +==============================+
	# |           Windows            |
	# +==============================+
	"Engine/Windows/Window_Base.rb",
	"Engine/Windows/Window_Selectable.rb",
	"Engine/Windows/Window_Command.rb",
	"Engine/Windows/Window_HorzCommand.rb",
	"Engine/Windows/Window_Help.rb",
	"Engine/Windows/Window_Gold.rb",
	"Engine/Windows/Window_MenuCommand.rb",
	"Engine/Windows/Window_MenuStatus.rb",
	"Engine/Windows/Window_MenuActor.rb",
	"Engine/Windows/Window_ItemCategory.rb",
	"Engine/Windows/Window_ItemList.rb",
	"Engine/Windows/Window_SkillCommand.rb",
	"Engine/Windows/Window_SkillStatus.rb",
	"Engine/Windows/Window_SkillList.rb",
	"Engine/Windows/Window_EquipStatus.rb",
	"Engine/Windows/Window_EquipCommand.rb",
	"Engine/Windows/Window_EquipSlot.rb",
	"Engine/Windows/Window_EquipItem.rb",
	"Engine/Windows/Window_Status.rb",
	"Engine/Windows/Window_SaveFile.rb",
	"Engine/Windows/Window_ShopCommand.rb",
	"Engine/Windows/Window_ShopBuy.rb",
	"Engine/Windows/Window_ShopSell.rb",
	"Engine/Windows/Window_ShopNumber.rb",
	"Engine/Windows/Window_ShopStatus.rb",
	"Engine/Windows/Window_NameEdit.rb",
	"Engine/Windows/Window_NameInput.rb",
	"Engine/Windows/Window_ChoiceList.rb",
	"Engine/Windows/Window_NumberInput.rb",
	"Engine/Windows/Window_KeyItem.rb",
	"Engine/Windows/Window_Message.rb",
	"Engine/Windows/Window_ScrollText.rb",
	"Engine/Windows/Window_MapName.rb",
	"Engine/Windows/Window_BattleLog.rb",
	"Engine/Windows/Window_PartyCommand.rb",
	"Engine/Windows/Window_ActorCommand.rb",
	"Engine/Windows/Window_BattleStatus.rb",
	"Engine/Windows/Window_BattleActor.rb",
	"Engine/Windows/Window_BattleEnemy.rb",
	"Engine/Windows/Window_BattleSkill.rb",
	"Engine/Windows/Window_BattleItem.rb",
	"Engine/Windows/Window_TitleCommand.rb",
	"Engine/Windows/Window_GameEnd.rb",
	"Engine/Windows/Window_DebugLeft.rb",
	"Engine/Windows/Window_DebugRight.rb",
	
	# +==============================+
	# |            Scenes            |
	# +==============================+
	"Engine/Scenes/Scene_Base.rb",
	"Engine/Scenes/Scene_Title.rb",
	"Engine/Scenes/Scene_Map.rb",
	"Engine/Scenes/Scene_MenuBase.rb",
	"Engine/Scenes/Scene_Menu.rb",
	"Engine/Scenes/Scene_ItemBase.rb",
	"Engine/Scenes/Scene_Item.rb",
	"Engine/Scenes/Scene_Skill.rb",
	"Engine/Scenes/Scene_Equip.rb",
	"Engine/Scenes/Scene_Status.rb",
	"Engine/Scenes/Scene_File.rb",
	"Engine/Scenes/Scene_Save.rb",
	"Engine/Scenes/Scene_Load.rb",
	"Engine/Scenes/Scene_End.rb",
	"Engine/Scenes/Scene_Shop.rb",
	"Engine/Scenes/Scene_Name.rb",
	"Engine/Scenes/Scene_Debug.rb",
	"Engine/Scenes/Scene_Battle.rb",
	"Engine/Scenes/Scene_Gameover.rb",
	
	# +==============================+
	# |        Yanfly Engine         |
	# +==============================+
	# "Engine/Materials/Element_Popups.rb",
	# "Engine/Materials/Enemy_Target_Info.rb",
	# "Engine/Materials/Enemy_HP_Bars.rb",
	# "Engine/Materials/Party_System.rb",
	
	# "Engine/Materials/Ace_Core_Engine.rb",
	# "Engine/Materials/Battle_Command_List.rb",
	# "Engine/Materials/Command_Party.rb",
	# "Engine/Materials/Input_Combo_Skills.rb"
	
	# +==============================+
	# |      Downloaded Scripts      |
	# +==============================+
	"WindowSkinChanger.rb"
	
	# +==============================+
	# |           My Files           |
	# +==============================+
]
SCRIPTS.each { |filename| require "#{Dir.getwd()}/Scripts/#{filename}" }

def main_entry_point
	print("Initializing game...\n")
	SceneManager.init()
	# vIndex = 0
	# global_variables().each do |global_var_name|
	# 	print("Var[%d]: %s %s\n" % [vIndex, global_var_name, "#{eval(global_var_name.to_s)}"])
	# 	vIndex += 1
	# end
	# print("Game System: %s" % [$game_system])
	
	# print("Adding shaders...\n")
	# mainShader = "
	# float my_time;
	# texture texture1;
	# sampler2D sampler1 = sampler_state
	# {
	# 	Texture = texture1;
	# 	AddressU = Wrap;
	# 	AddressV = Wrap;
	# };
	# PS_OUTPUT TestShaderMain(float4 color : COLOR0, float2 texcoord : TEXCOORD0)
	# {
	# 	float4 res = samp(texcoord);
	# 	//float4 res = tex2D(sampler1, texcoord);
	# 	//res = ColorMap(res);
	# 	//res = BushMap(res, texcoord);
	# 	//res *= color;
	# 	//res = ToneMap(res);
	# 	res.rgb *= res.a;
	# 	res.r = 1 - res.r;
	# 	return GetOutput(res);
	# }
	# PS_OUTPUT WavyShaderMain(float4 color : COLOR0, float2 texcoord : TEXCOORD0)
	# {
	# 	float2 normalTexCoord = float2(texcoord.x + cos(my_time)*0.1, texcoord.y + sin(my_time)*0.1);
	# 	float4 normal = tex2D(sampler1, normalTexCoord);
	# 	float4 res = samp(texcoord + float2(normal.r-0.5, normal.g-0.5)*0.01);
	# 	res = ColorMap(res);
	# 	res = BushMap(res, texcoord);
	# 	res *= color;
	# 	res = ToneMap(res);
	# 	res.rgb *= res.a;
	# 	//res.r = (sin(my_time * PI) + 1)/2;
	# 	//res.r = normal.r;
	# 	return GetOutput(res);
	# }
	# PS_OUTPUT OutlineShaderMain(float4 color : COLOR0, float2 texcoord : TEXCOORD0)
	# {
	# 	float4 res = samp(texcoord);
	# 	float4 right = samp(float2(texcoord.x + 0.01, texcoord.y));
	# 	float4 left  = samp(float2(texcoord.x - 0.01, texcoord.y));
	# 	float4 up    = samp(float2(texcoord.x, texcoord.y + 0.01));
	# 	float4 down  = samp(float2(texcoord.x, texcoord.y - 0.01));
	# 	float diff = abs(res.r - right.r) + abs(res.g - right.g) + abs(res.b - right.b);
	# 	//res = ColorMap(res);
	# 	//res = BushMap(res, texcoord);
	# 	//res *= color;
	# 	//res = ToneMap(res);
	# 	//res.rgb *= res.a;
	# 	float sinValue = (sin(my_time) + 1) / 2;
	# 	//return GetOutput(float4(sinValue, sinValue, sinValue, 1));
	# 	if (diff < 0.7) { return GetOutput(float4(0, 0, 0, res.a)); }
	# 	else { return GetOutput(float4(0, 1, 0, res.a)); }
	# }"
	# passShader = "
	# pass TestShader
	# {
	# 	AlphaBlendEnable = true;
	# 	SeparateAlphaBlendEnable = true;
		
	# 	BlendOp = ADD;
	# 	SrcBlend = ONE;
	# 	DestBlend = INVSRCALPHA;
	# 	SrcBlendAlpha = ONE;
	# 	DestBlendAlpha = INVSRCALPHA;
		
	# 	PixelShader = compile ps_2_0 TestShaderMain();
	# }
	# pass WavyShader
	# {
	# 	AlphaBlendEnable = true;
	# 	SeparateAlphaBlendEnable = true;
		
	# 	BlendOp = ADD;
	# 	SrcBlend = ONE;
	# 	DestBlend = INVSRCALPHA;
	# 	SrcBlendAlpha = ONE;
	# 	DestBlendAlpha = INVSRCALPHA;
		
	# 	PixelShader = compile ps_2_0 WavyShaderMain();
	# }
	# pass OutlineShader
	# {
	# 	AlphaBlendEnable = true;
	# 	SeparateAlphaBlendEnable = true;
		
	# 	BlendOp = ADD;
	# 	SrcBlend = ONE;
	# 	DestBlend = INVSRCALPHA;
	# 	SrcBlendAlpha = ONE;
	# 	DestBlendAlpha = INVSRCALPHA;
		
	# 	PixelShader = compile ps_2_0 OutlineShaderMain();
	# }"
	# Graphics.add_shader(mainShader, passShader)
	
	# print(Time.now.to_i)
	
	print("Starting game!\n")
	SceneManager.run
end

rgss_main { main_entry_point }
