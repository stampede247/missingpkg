# This file holds a bunch of functionst that we can call from anywhere
# and help us perform common tasks

def Print_D(message)
  print(message.to_s)
end
def PrintLine_D(message)
  print(message.to_s + "\n")
end

def print_self(instance)
	instance_vars = instance_variables()
	PrintLine_D("#{instance}:")
	instance_vars.each do |var_name|
		PrintLine_D("  #{var_name} = #{eval(var_name.to_s)}")
	end
end